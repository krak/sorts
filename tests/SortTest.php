<?php

namespace Krak\Tests;

use Krak\Sorts;

class SortTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider sortsProvider
     */
    public function testConstruct($obj)
    {
        $this->assertInstanceOf(
            'Krak\Sorts\Sort',
            $obj
        );
    }

    public function testMockSortConstruct()
    {
        $this->assertInstanceOf(
            Sorts\MockSort::class,
            new Sorts\MockSort([])
        );
    }
    public function testStableSortConstruct()
    {
        $this->assertInstanceOf(
            Sorts\StableSort::class,
            new Sorts\StableSort(new Sorts\IdentitySort(), new Sorts\IdentitySort(), 2)
        );
    }

    /**
     * @dataProvider sortDataProvider
     */
    public function testSorts($sort, $orig, $expected)
    {
        $sort->sort($orig, 'strcmp');
        $this->assertEquals($expected, $orig);
    }

    public function sortDataProvider()
    {
        $ms = new Sorts\MergeSort();
        $ss = new Sorts\StableSort(new Sorts\MockSort(['a']), new Sorts\MockSort(['b']), 2);
        return [
            [new Sorts\IdentitySort(), ['b', 'a'], ['b', 'a']],
            [new Sorts\MockSort([]), ['b'], []],
            [new Sorts\InsertionSort(), ['b', 'a'], ['a', 'b']],
            [$ms, ['a'], ['a']],
            [$ms, ['a', 'b'], ['a', 'b']],
            [$ms, ['b', 'a', 'c'], ['a', 'b', 'c']],
            [$ss, ['c'], ['a']],
            [$ss, ['c', 'c'], ['b']],
        ];
    }

    /**
     * @dataProvider staticSortsProvider
     */
    public function testStaticSorts($func)
    {
        $this->assertSame($func(), $func());
    }

    public function staticSortsProvider()
    {
        return [
            ['krak\sorts\getInsertionSort'],
            ['krak\sorts\getMergeSort'],
            ['krak\sorts\getStableSort']
        ];
    }

    /**
     * @dataProvider sortFunctionsProvider
     */
    public function testSortFunctions($func, $orig, $expected)
    {
        $func($orig);
        $this->assertEquals($expected, $orig);
    }

    public function sortFunctionsProvider()
    {
        $sorts = ['krak\sorts\stable', 'krak\sorts\merge', 'krak\sorts\insertion'];
        return array_map(function($sort)
        {
            return [$sort, ['b', 'a'], ['a', 'b']];
        }, $sorts);
    }

    /**
     * @dataProvider compareFunctionsProvider
     */
    public function testCompareFunctions($func, $a, $b, $expected)
    {
        $this->assertEquals($expected, $func($a, $b));
    }

    public function compareFunctionsProvider()
    {
        return [
            [sorts\PHPCMP, 1, 2, -1],
            [sorts\PHPCMP, 1, 1, 0],
            [sorts\PHPCMP, 2, 1, 1],
            [sorts\INTCMP, 1, 1, 0],
        ];
    }

    public function sortsProvider()
    {
        return [
            [new Sorts\IdentitySort()],
            [new Sorts\MockSort([])],
            [new Sorts\InsertionSort()],
            [new Sorts\MergeSort()],
            [new Sorts\StableSort(new Sorts\IdentitySort(), new Sorts\IdentitySort(), 25)]
        ];
    }
}
