<?php

namespace Krak\Sorts;

class MockSort implements Sort
{
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function sort(&$vals, $cmp)
    {
        $vals = $this->data;
    }
}
