<?php

namespace Krak\Sorts;

use SplFixedArray;

/**
 * Space efficient merge sort algorithm
 */
class MergeSort implements Sort
{
    private function merge(&$vals, $start, $middle, $end, $tuple)
    {
        list($cmp, $buf, $buf_count) = $tuple;
        $l = $start;
        $r = $middle;
        $buf_end_idx = 0;
        $buf_start_idx = 0;
        $tmp;

        for ($i = $start; $i < $end; $i++) {
            if ($cmp($vals[$l], $vals[$r]) > 0) {
                $buf[$buf_end_idx % $buf_count] = $vals[$r];

                $r++;
                $buf_end_idx++;
            }
            else {
                if ($buf_end_idx == $buf_start_idx) {
                    $l++;
                }
                else {
                    $tmp = $vals[$l];
                    $vals[$l] = $buf[$buf_start_idx % $buf_count];
                    $buf[$buf_end_idx % $buf_count] = $tmp;

                    $l++;
                    $buf_start_idx++;
                    $buf_end_idx++;
                }
            }

            if ($l == $middle || $r == $end) {
                break;
            }
        }

        for ($i = 0; $i < $buf_end_idx - $buf_start_idx; $i++) {
            $vals[$middle + $i] = $vals[$l + $i];
            $vals[$l + $i] = $buf[($buf_start_idx + $i) % $buf_count];
        }
    }

    private function mergeSort(&$vals, $start, $end, $tuple)
    {
        if ($end - $start <= 1) {
            return;
        }

        $middle = (int) (($end + $start) / 2);
        $this->mergeSort($vals, $start, $middle, $tuple);
        $this->mergeSort($vals, $middle, $end, $tuple);
        $this->merge($vals, $start, $middle, $end, $tuple);
    }

    public function sort(&$vals, $cmp)
    {
        if (count($vals) < 2) {
            return;
        }

        /* half + 1 or 2 to hold swap variables */
        if (count($vals) % 2 == 0) {
            $buf_count = count($vals) / 2;
        }
        else {
            $buf_count = count($vals) / 2 + 1;
        }

        $buf = new SplFixedArray($buf_count);

        $this->mergeSort($vals, 0, count($vals), [$cmp, $buf, $buf_count]);
    }
}
