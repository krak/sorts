<?php

namespace Krak\Sorts;

use Closure;

interface Sort
{
    /**
     * @param array &$vals
     * @param string|Closure $cmp $cmp is called via $cmp()
     */
    public function sort(&$vals, $cmp);
}
