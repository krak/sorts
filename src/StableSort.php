<?php

namespace Krak\Sorts;

/**
 * Run a stable sorting algorithm. Switch to insertion or merge depending
 * on the size of the array for better performance.
 */
class StableSort implements Sort
{
    private $few_sort;
    private $many_sort;
    private $cutoff;

    public function __construct(Sort $few_sort, Sort $many_sort, $cutoff)
    {
        $this->few_sort = $few_sort;
        $this->many_sort = $many_sort;
        $this->cutoff = $cutoff;
    }

    public function sort(&$vals, $cmp)
    {
        /* insertion sort will typically outperform merge when under 25 */
        if (count($vals) < $this->cutoff) {
            $this->few_sort->sort($vals, $cmp);
        }
        else {
            $this->many_sort->sort($vals, $cmp);
        }
    }
}
