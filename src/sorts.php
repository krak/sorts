<?php

namespace Krak\Sorts;

use Closure,
    SplFixedArray;

const INTCMP = 'krak\sorts\intcmp';
const STRCMP = 'strcmp';
const PHPCMP = 'krak\sorts\phpcmp';

function intcmp($a, $b)
{
    return $a - $b;
}

function phpcmp($a, $b)
{
    if ($a < $b) {
        return -1;
    }
    else if ($a > $b) {
        return 1;
    }
    else {
        return 0;
    }
}

/**
 * Returns a global instance of the stable sort
 */
function getStableSort()
{
    static $stable;

    if ($stable) {
        return $stable;
    }

    $stable = new StableSort(getInsertionSort(), getMergeSort(), 25);
    return $stable;
}

function getMergeSort()
{
    static $merge;

    if ($merge) {
        return $merge;
    }

    $merge = new MergeSort();

    return $merge;
}

function getInsertionSort()
{
    static $insertion;

    if ($insertion) {
        return $insertion;
    }

    $insertion = new InsertionSort();

    return $insertion;
}

function stable(&$vals, $cmp = PHPCMP)
{
    getStableSort()->sort($vals, $cmp);
}

function insertion(&$vals, $cmp = PHPCMP)
{
    getInsertionSort()->sort($vals, $cmp);
}

function merge(&$vals, $cmp = PHPCMP)
{
    getMergeSort()->sort($vals, $cmp);
}
