<?php

namespace Krak\Sorts;

class InsertionSort implements Sort
{
    public function sort(&$vals, $cmp)
    {
        for ($i = 1; $i < count($vals); $i++) {
            $x = $vals[$i];
            $j = $i;
            while ($j > 0 && $cmp($vals[$j - 1], $x) > 0) {
                $vals[$j] = $vals[$j - 1];
                $j--;
            }
            $vals[$j] = $x;
        }
    }
}
